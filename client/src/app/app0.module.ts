import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

//import { TasksComponent } from './components/tasks/tasks.component';
import { CrisisListComponent }  from './components/crisis/crisis-list.component';
import { HeroesModule }         from './components/hero/heroes.module';


@NgModule({
  declarations: [
    AppComponent,
    CrisisListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    HeroesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
