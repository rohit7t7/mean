import { Component } from '@angular/core';
import { TasksComponent } from './components/tasks/tasks.component';
import { TaskserviceService } from './services/taskservice.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  directives:[TasksComponent],
  providers:[TaskserviceService]
})
export class AppComponent {
  title = 'app works!';
}
