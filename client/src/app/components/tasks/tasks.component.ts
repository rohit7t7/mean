import { Component, OnInit } from '@angular/core';
import { TaskserviceService } from '../../services/taskservice.service';
import { Task } from './task';
@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
tasks : Task[];
first_name :  string;
last_name :  string;
gender :  string;
constructor(private taskservice:TaskserviceService) {

 }


  ngOnInit() {
  this.taskservice.getTasks().subscribe(tasks=>{
    this.tasks=tasks;
  });
  }

  addTask(event){
    event.preventDefault();
    var newUser={
      first_name  :this.first_name,
      last_name   :this.last_name,
      gender      :this.gender
    }
    this.taskservice.addTask(newUser).subscribe(task=>{
      this.tasks.push(newUser);
    });
  }

  deleteTask(id){
    this.taskservice.deleteTask(id).subscribe(tasks=>{
     console.log("Deleted");
    });
  }


}
