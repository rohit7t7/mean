import { Component } from '@angular/core';

/**
 * This class represents the NavigationComponent.
 */
@Component({
  selector: 'vj-navigation',
  templateUrl: 'navigation.component.html',
  styleUrls: ['navigation.component.scss']
})
export class NavigationComponent {}
