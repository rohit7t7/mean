import { Component } from '@angular/core';

/**
 * This class represents the header component.
 */
@Component({
  selector: 'vj-header',
  templateUrl: 'header.component.html'
})
export class HeaderComponent {}
