import { Component } from '@angular/core';

/**
 * This class represents the header component.
 */
@Component({
  selector: 'vj-footer',
  templateUrl: 'footer.component.html',
  styleUrls: ['footer.component.scss']
})
export class FooterComponent {}
