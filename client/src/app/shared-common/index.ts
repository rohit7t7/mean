/**
 * This barrel file provides the exports for the shared resources (services, components).
 */
export * from './header/index';
export * from './navigation/index';
export * from './footer/index';
