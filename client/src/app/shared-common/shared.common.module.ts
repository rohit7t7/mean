import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './header/index';
import { NavigationComponent } from './navigation/index';
import { FooterComponent } from './footer/index';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [HeaderComponent, NavigationComponent, FooterComponent],
  exports: [HeaderComponent, NavigationComponent, FooterComponent, CommonModule, FormsModule, RouterModule]
})
export class SharedCommonModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedCommonModule,
      providers: []
    };
  }
}
