import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { AppComponent }      from './app.component';
import { AppRoutingModule }  from './app-routing.module';
import { HeroesModule }         from './components/hero/heroes.module';
import { CrisisListComponent }  from './components/crisis/crisis-list.component';

import { SharedCommonModule } from './shared-common/shared.common.module';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HeroesModule,
    AppRoutingModule,
    SharedCommonModule.forRoot()
  ],
  declarations: [
    AppComponent,
    CrisisListComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
