/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TaskserviceService } from './taskservice.service';

describe('TaskserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaskserviceService]
    });
  });

  it('should ...', inject([TaskserviceService], (service: TaskserviceService) => {
    expect(service).toBeTruthy();
  }));
});
