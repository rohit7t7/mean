import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class TaskserviceService {

  constructor(private http:Http) {
      console.log("Task service initialize..");
   }

   getTasks(){
    return this.http.get('http://localhost:4200/api/tasks').map(res => res.json());
   }

   addTask(newUser){
    var headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:4200/api/task',JSON.stringify(newUser),{headers}).map(res=>res.json());
   }

   deleteTask(id){
    return this.http.delete('http://localhost:4200/api/task/'+id).map(res=>res.json());
   }

}
