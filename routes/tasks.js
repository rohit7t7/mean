var express       =   require('express');
var router        =   express.Router();
var mongojs       =   require('mongojs');
//var db            =   mongojs("mongodb://brad:brad@localhost:27017/mycustomers",['customers']);

  // Retrieve
  var MongoClient = require('mongodb').MongoClient;
  // Connect to the db
  MongoClient.connect("mongodb://localhost:27017/mycustomers", function(err, db) {
    var collection = db.collection('customers');

    router.get('/tasks',function(req,res,next){
       collection.find().toArray(function(err, items) {
         console.log(items);
         res.send(items);
       });
    });

    router.get('/tasks/:id',function(req,res,next){
       collection.findOne({_id:mongojs.ObjectId(req.params.id)},function(err, item) {
         console.log(item);
         res.send(item);
       });
    });

    router.post('/task',function(req,res,next){
      var task=req.body;
      console.log(req.body);
      if(!task.first_name || !task.last_name){
        console.log("errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr",req);
        res.status(400);
        res.json({
          "error":"Bad data"
        });
      }else{
        collection.save(task,function(err,task){
          if(err){
            res.send(err);
          }else{
            console.log("Task:"+JSON.stringify(task));
            res.send(task)
          }
        });
      }
    });

    router.delete('/task/:id',function(req,res,next){
       collection.remove({_id:mongojs.ObjectId(req.params.id)},function(err, item) {
         console.log(item);
         res.send(item);
       });
    });
    // console.log("hitting");

    router.put('/tasks/:id',function(req,res,next){
      var task = req.body;
      var updtask={};
      if(task.first_name){
        updtask.first_name=task.first_name;
      }
      if(task.last_name){
        updtask.first_name=task.first_name;
      }
      if(!updtask){
        res.status(400);
        res.json({
          "error":"Bad data"
        });
      }else{
        collection.update({_id:mongojs.ObjectId(req.params.id)},updtask,{},function(err, item) {
          if(err){
            res.send(err);
          }else{
            res.send(item)
          }
        });
      }
    });
  });

//});

module.exports=router;
